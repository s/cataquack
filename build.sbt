import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

scalaVersion in ThisBuild := "2.12.8"

val compilerOptions = Seq(
  "-unchecked",
  "-deprecation",
  "-feature",
  "-language:higherKinds",
  "-Ypartial-unification",
)

val mainSettings = Seq(
  name         := "cataquack",
  organization := "tf.bug",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
  scalacOptions ++= compilerOptions,
  libraryDependencies ++= Seq(
    "org.typelevel" %%% "cats-core"   % "1.5.0",
    "org.typelevel" %%% "cats-effect" % "1.1.0",
  ),
  resolvers += Resolver.sonatypeRepo("releases"),
  addCompilerPlugin("org.spire-math" % "kind-projector" % "0.9.8" cross CrossVersion.binary)
)

lazy val cataquack =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Pure)
    .settings(mainSettings)
    .jsSettings(crossScalaVersions := Seq("2.11.12", "2.12.8"))
    .jvmSettings(crossScalaVersions := Seq("2.11.12", "2.12.8"))

lazy val cataquackJS = cataquack.js
lazy val cataquackJVM = cataquack.jvm

val jsonDirSettings = Seq(
  name         := "cataquack-jsondir",
  organization := "tf.bug",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
  scalacOptions ++= compilerOptions,
  libraryDependencies ++= Seq(
    "io.circe" %%% "circe-core"    % "0.11.0",
    "io.circe" %%% "circe-generic" % "0.11.0",
    "io.circe" %%% "circe-parser"  % "0.11.0",
  ),
  resolvers += Resolver.sonatypeRepo("releases"),
  addCompilerPlugin("org.spire-math" % "kind-projector" % "0.9.8" cross CrossVersion.binary)
)

lazy val cataquackJsonDir =
  crossProject(JVMPlatform)
    .crossType(CrossType.Pure)
    .settings(jsonDirSettings)
    .jvmSettings(crossScalaVersions := Seq("2.11.12", "2.12.8"))
    .dependsOn(cataquack)

lazy val cataquackJsonDirJVM = cataquackJsonDir.jvm
