package tf.bug.cataquack

import cats.Functor

/**
  * A case class meant to be used under IORaw (see package.scala) like so:
  * {{{
  *   type StringIORaw[T] = IORaw[T, String]
  * }}}
  * @param s A concrete value that has been retrieved
  * @tparam T The target type
  * @tparam S The raw type
  */
case class Raw[S, T](s: S) {

  def read(implicit readEv: Read[S, T]): T = readEv.apply(s)

  def map[U](f: T => U): Raw[S, U] = Raw[S, U](s)

}

trait RawImplicits {

  implicit def rawFunctor[S]: Functor[Raw[S, ?]] = new Functor[Raw[S, ?]] {
    override def map[A, B](fa: Raw[S, A])(f: A => B): Raw[S, B] = fa.map(f)
  }

}
