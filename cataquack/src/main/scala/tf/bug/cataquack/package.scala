package tf.bug

import cats.effect.IO

package object cataquack {

  type IORaw[S, T] = IO[Raw[S, T]]

}
