package tf.bug.cataquack

import cats._
import cats.implicits._

trait Storage[F[_], C[_]] {

  def query[T](key: String): F[T]

  def map[A, B](fa: F[A])(f: A => B)(implicit functor: Functor[F]): F[B] = fa.map(f)
  def flatMap[A, B](fa: F[A])(f: A => F[B])(implicit flatMap: FlatMap[F]): F[B] = fa.flatMap(f)

  def filter[T](ft: F[T])(f: T => Boolean)(implicit filterFunctor: FunctorFilter[F]): F[T] = ft.filter(f)

  /** Type like List, Id, etc */
  type Output[T]
  def execute[T: C](f: F[T]): this.Output[T]

}
