package tf.bug.cataquack

trait Read[I, O] {

  def apply(i: I): O

}

trait ReadImplicits {

  implicit def readIdentity[T]: Read[T, T] = identity[T]

}
