package tf.bug.cataquack.jsondir
import io.circe.{Decoder, Json}
import io.circe.syntax._
import tf.bug.cataquack.Read

object implicits {

  implicit def readDecoder[T: Decoder]: Read[Json, T] = (i: Json) => i.as[T].right.get

}
