package tf.bug.cataquack.jsondir

import implicits._
import java.nio.file.Path

import cats.effect.IO
import io.circe._
import io.circe.parser._
import tf.bug.cataquack.{IORaw, Raw, Storage}

import scala.io.Source
import scala.util.Try

object JSONDirectoryStorage {

  def apply(p: Path): Try[Storage[IORaw[Json, ?], Decoder]] = Try {
    if (p.toFile.isDirectory) {
      new Storage[IORaw[Json, ?], Decoder] {

        override def query[T](key: String): IORaw[Json, T] = IO {
          val nf = p.resolve(key).toFile
          val s = Source.fromFile(nf).mkString
          val j = parse(s).right.get
          Raw[Json, T](j)
        }

        /** Type like List, Id, etc */
        override type Output[T] = IO[T]
        override def execute[T: Decoder](
          f: IORaw[Json, T]
        ): Output[T] = {
          f.map(_.read)
        }

      }
    } else {
      throw new IllegalArgumentException("Supplied path must be a directory!")
    }
  }

}
